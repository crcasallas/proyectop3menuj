import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading:boolean = false;
  loginForm: FormGroup = this.fb.group( {
    usuario: ['', Validators.required],
    password: ['', Validators.required]
  })

  constructor(private fb:FormBuilder, private _snackBar: MatSnackBar, private router: Router) { }

  ngOnInit(): void {
  }

  ingresar() {
    const usuario =this.loginForm.value.usuario;
    const password =this.loginForm.value.password;

    if(usuario == 'crcasallas' && password == '123456'){
      localStorage.setItem("token", '1234567');
      //redirecciona al dashboard
      this.fakeLoading();
    }else{
      //mostrar mensaje de error
      this.error();
      this.loginForm.reset();
    }
  }

  error(){
    this._snackBar.open('Usuario y password son incorrectos', '', {
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }

  fakeLoading(){
    this.loading = true;
    setTimeout(() => {      
      this.router.navigate(['pages'])   
    }, 1500);
  }

}
