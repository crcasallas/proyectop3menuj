import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaGridComponent } from './lista-grid/lista-grid.component';
import { ListaTablaComponent } from './lista-tabla/lista-tabla.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  { path:'pages', component:PagesComponent,
      children: [       
          // { path:'', component:PagesComponent },   
          { path:'listatabla', component:ListaTablaComponent },
          { path:'listagrid', component:ListaGridComponent },
          { path:'**', redirectTo:'listatabla', pathMatch:'full'}
      ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
