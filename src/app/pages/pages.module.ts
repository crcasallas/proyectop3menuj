import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ListaTablaComponent } from './lista-tabla/lista-tabla.component';
import { ListaGridComponent } from './lista-grid/lista-grid.component';
import { ModalComponent } from './modal/modal.component';
import { MaterialModule } from '../material.module';


@NgModule({
  declarations: [
    PagesComponent,    
    ListaTablaComponent,
    ListaGridComponent,
    ModalComponent
  ],
  exports: [
    PagesComponent,    
    ListaTablaComponent,
    ListaGridComponent,
    ModalComponent
  ],
  imports: [
    PagesRoutingModule,
    CommonModule,    
    MaterialModule,
    RouterModule
  ]
})
export class PagesModule { }
